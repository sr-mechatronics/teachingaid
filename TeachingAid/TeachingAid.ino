/*
    Servo to follow red and yellow person identified with pixy
*/

#include <Servo.h> // Include the servo library
#include <SPI.h>
#include <Pixy.h>
#include <Wire.h>

Servo servo;
Pixy pixy;

const int red = 1;
const int yellow = 2;
const int purple = 3;
const int MotorPin = 47; // Assign pin number for motor
int MotorAngle = 90;
const int CenteringError = 10;
int d = 1;
boolean podium = false;
boolean gesture = false;
const int PodiumAngle = 130; // Determine angle to OUTER EDGE of the podium (closest to the projector)
//const int PodiumError = 10;
int LeftPodium = 0;
int totalLeftPodium = 0;
int MakeGesture = 0;
int totalMakeGesture = 0;
boolean back = false;
const int MaxAngle = 160;
const int MinAngle = 60;
unsigned long timeNotFacingClass = 0;
unsigned long totalTimeNotFacingClass = 0;
unsigned long prevTime = millis();
unsigned long prevResults = millis();
unsigned long deltaTime;
const long TEN_MIN = 60*1000*10;
const long TWE_MIN = TEN_MIN*2;
const long THI_MIN = TEN_MIN*3;
boolean after10Min = false;
boolean after20Min = false;
boolean after30Min = false;
int barGestures = 0;
int barPodium = 0;
int barFacing = 0;
int MaxGestures = 10;
int MaxPodium = 10;


void setup() {
  Serial.begin(9600); // Begin the serial monitor
  Serial.println("Beginning detection...");
  pixy.init();
  servo.attach(MotorPin); // Attach the motor pin to the servo
  servo.write(MotorAngle); // Start motor at angle of 0 degrees
  Wire.begin();
  prevResults = millis();
}

void loop() {
  static int i = 0;
  static int j = 0;
  static int k = 0;
  uint16_t blocks;
  int largest_red = 5;
  int red_index = -1;
  int largest_yellow = 5;
  int yellow_index = -1;
  int index = 0;
  boolean foundRedOrYel = false;
  boolean nobody = false;
  boolean foundPurple = false;
  boolean foundRed = false;
  boolean foundYellow = false;

  // get num blocks
  blocks = pixy.getBlocks();
  if (blocks) {
    k = 0;
    // loop over blocks. extract largest red and yellow blocks. cehck if purple in the frame
    for (i = 0; i < blocks; i++) {
      if ((pixy.blocks[i].signature == red) && (pixy.blocks[i].width > largest_red)) {
        largest_red = pixy.blocks[i].width;
        red_index = i;
        //nobody = false;
        foundRedOrYel = true;
      }
      else if ((pixy.blocks[i].signature == yellow) && (pixy.blocks[i].width > largest_yellow)) {
        largest_yellow = pixy.blocks[i].width;
        yellow_index = i;
        //nobody = false;
        foundRedOrYel = true;
      }
      if (pixy.blocks[i].signature == purple) {
        foundPurple = true;
      }
    }
    if (foundPurple && !gesture) {
      MakeGesture++;
      //Serial.print("Called on student: ");
      //Serial.println(MakeGesture);
      gesture = true;
      j = 0;
    }
    if (!foundPurple) {
      j++;
      if (j % 65 == 0) {
        gesture = false;
        j = 0;
      }
    }

    // determine whether the yellow or red block is large (and therefore closer)
    // set the index tot he largest one then center on that
    if (foundRedOrYel) { // If red or yellow identified (body identified)
      if (largest_red > largest_yellow) { // Determine if red or yellow is larger (center on larger)
        index = red_index;
        //Serial.println("found red");
        foundRed = true;
      } else {
        index = yellow_index;
        foundYellow = false;
      }
      // center the serve on the largest found one.
      while (abs(pixy.blocks[index].x - 160) > CenteringError) { // while center in the x is more than 10 from the center (160) in either direction
        if (pixy.blocks[index].x < 160 - CenteringError) {
          d = 1;
          MotorAngle++;
          if (MotorAngle > MaxAngle) {
            MotorAngle = MaxAngle;
          }
          servo.write(MotorAngle);
        } else if (pixy.blocks[index].x > 160 + CenteringError) {
          d = -1;
          MotorAngle--;
          if (MotorAngle < MinAngle) {
            MotorAngle = MinAngle;
          }
          servo.write(MotorAngle);
        }
        blocks = pixy.getBlocks();
        if (!blocks) {
          break;
        }
      }
      // if the current motor angle is less than the podium angle (calibrated before run)
      // then the teacher is behind the podium
      if (MotorAngle <= PodiumAngle) { // Not behind podium
        if (podium) { // Last seen behind podium
          podium = false;
          LeftPodium++; // Number of times left the podium counter
          //Serial.print("Left podium: ");
          //Serial.println(LeftPodium);
        }
      } else { // Behind podium
        if (!podium) { // Last seen not behind podium
          podium = true;
        }
      }
    }
  }
  // if nothing is found then after a certain number of iterations start scanning
  // ie the serve pans back and forth
  if (!blocks) {
    k++;
    if (k % 5000 == 0) {
      nobody = true;
    }
  }
  // scan
  if (nobody) { // No body identified (block of different color or no blocks)
    // scan endlessly - go one degree and check
    k++;
    while (!blocks) {
      blocks = pixy.getBlocks();
      MotorAngle = MotorAngle + d;
      if ((MotorAngle < MinAngle) || (MotorAngle > MaxAngle)) {
        d = -1 * d;
        MotorAngle = MotorAngle + d;
      }
      servo.write(MotorAngle);
      delay(20);
      k = 0;
    }
  }
  // if the teacher was pervious not facing the wall and now is, start tracking the amount of time
  if (foundRed && !back) {
    //Serial.println("turned to back. keeping track of back time.");
    prevTime = millis();
    back = true;
  }
  // if the teacher was facing the wall and still is, increment the time counter by delta time
  else if (foundRed && back) {
    //Serial.print("still facing the board. incrementing time (millis):\t");
    deltaTime = millis() - prevTime;
    //Serial.println(deltaTime);
    prevTime = millis();
    timeNotFacingClass += deltaTime;
  }
  // if the teacher is now facing the students, increment nothing and set was not previously facing the wall
  else if (foundYellow) {
    // may need to add in counter delay
    //Serial.println("resetting");
    back = false;
  }
  // READOUTS
  if (millis() > TEN_MIN && !after10Min) {
    Serial.print("\n*** 10 MINUTE MARK ***\nPercentage of time facing class: ");
    double percent_not_facing = ((double) timeNotFacingClass)
                                / ((double)(millis()));
    Serial.print(abs(1.0 - percent_not_facing) * 100.0, 2);
    Serial.println(" %");
    Serial.print("Number of times left podium: ");
    Serial.println(LeftPodium);
    Serial.print("Number of times called on student: ");
    Serial.println(MakeGesture);
    after10Min = true;
    totalTimeNotFacingClass += timeNotFacingClass;
    totalLeftPodium += LeftPodium;
    totalMakeGesture += MakeGesture;
    barGestures = round(constrain(MakeGesture*1.0/MaxGestures*10,0,10));
    barPodium = round(constrain(LeftPodium*1.0/MaxPodium*10,0,10));
    barFacing = round(constrain((1.0 - percent_not_facing) * 10.0,0,10));
    // Send to other pixy. address=8
    Wire.beginTransmission(8);
    Wire.write(barGestures);
    Wire.write(barPodium);
    Wire.write(barFacing);
    Wire.endTransmission();
    timeNotFacingClass = 0;
    LeftPodium = 0;
    MakeGesture = 0;
    prevResults = millis();
  }
  if (millis() > TWE_MIN && !after20Min) {
    
    Serial.print("\n*** 20 MINUTE MARK ***\nPercentage of time facing class: ");
    double percent_not_facing = ((double) timeNotFacingClass)
                                / ((double)(millis() - prevResults));
    Serial.print(abs(1.0 - percent_not_facing) * 100.0, 2);
    Serial.println(" %");
    Serial.print("Number of times left podium: ");
    Serial.println(LeftPodium);
    Serial.print("Number of times called on student: ");
    Serial.println(MakeGesture);
    after20Min = true;
    totalTimeNotFacingClass += timeNotFacingClass;
    totalLeftPodium += LeftPodium;
    totalMakeGesture += MakeGesture;
    barGestures = round(constrain(MakeGesture*1.0/MaxGestures*10,0,10));
    barPodium = round(constrain(LeftPodium*1.0/MaxPodium*10,0,10));
    barFacing = round(constrain((1.0 - percent_not_facing) * 10.0,0,10));
    Wire.beginTransmission(8);
    Wire.write(barGestures);
    Wire.write(barPodium);
    Wire.write(barFacing);
    Wire.endTransmission();
    timeNotFacingClass = 0;
    LeftPodium = 0;
    MakeGesture = 0;
    prevResults = millis();
  }
  if (millis() > THI_MIN && !after30Min) {
    Serial.print("\n*** 30 MINUTE MARK ***\nPercentage of time facing class: ");
    double percent_not_facing = ((double) timeNotFacingClass)
                                / ((double)(millis() - prevResults));
    Serial.print(abs(1.0 - percent_not_facing) * 100.0, 2);
    Serial.println(" %");
    Serial.print("Number of times left podium: ");
    Serial.println(LeftPodium);
    Serial.print("Number of times called on student: ");
    Serial.println(MakeGesture);
    after30Min = true;
    totalTimeNotFacingClass += timeNotFacingClass;
    totalLeftPodium += LeftPodium;
    totalMakeGesture += MakeGesture;
    barGestures = round(constrain(MakeGesture*1.0/MaxGestures*10,0,10));
    barPodium = round(constrain(LeftPodium*1.0/MaxPodium*10,0,10));
    barFacing = round(constrain((1.0 - percent_not_facing) * 10.0,0,10));
    Wire.beginTransmission(8);
    Wire.write(barGestures);
    Wire.write(barPodium);
    Wire.write(barFacing);
    Wire.endTransmission();
    timeNotFacingClass = 0;
    LeftPodium = 0;
    MakeGesture = 0;
    Serial.print("\n\n*** TOTALS ***\nTotal percentage of time facing class: ");
    Serial.print(abs(1 - (totalTimeNotFacingClass * 1.0) / millis()) * 100, 2);
    Serial.println(" %");
    Serial.print("Total number of times left podium: ");
    Serial.println(totalLeftPodium);
    Serial.print("Total number of times called on student: ");
    Serial.println(totalMakeGesture);
    while(1){} // loop FOREVER!
  }
}
