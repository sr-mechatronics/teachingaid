#JHU Mechatronics (530.421) - Spring 2016

Cora Dimmig

@sreichenstein

Baxter Eldridge

Michael Patti



Laboratory 6 - Teaching Aid
===========================

##*Objective*
It was our objective to create a complete mechatronics system that provides a professor feedback on their lecture in ten minute increments.  The professor is evaluated in three categories: percentage of time they face the students, amount of student involvement, and the number of times they leave the podium.  

##*Solution*
The system developed to evaluate the professor’s performance consists of a Pixy camera attached to a servo motor mounted on a tripod and an LED gauge to report the professor’s performance.  The Pixy camera tracks the yellow and red on the professors front and back respectively.  Commands are sent to the servo motor to center on the yellow or red that is identified.  If neither is identified of a reasonable size, the Pixy camera scans the room until the professor can be identified again.  The number of times the professor left the podium is based on the angle of the camera while the professor is being tracked.  Specifically any angles greater than a predetermined podium angle count as the corner where the podium is and angles less than this podium angle is the rest of the room.

The percent of time the professor is facing the class is determined by tracking the current color (red or yellow) with the largest area and when this color value changes.  We counted the amount of time the professor is not facing the class (since we figured this would be less frequent) and divided it by the total time.  Subtracting this value from 1 and multiplying by 100 gave the percentage of time the professor is facing the class.
  
Student involvement is tracked by counting the number of times the purple glove is seen when a gesture was not already recorded as taking place.  These three measured quantities are then graphically displayed on the LED gauge in 3 bars.  The LED gauge consists of 30 green LEDs Charlieplexed, a form of multiplexing specifically for LEDs (see [Charlieplexing](https://en.wikipedia.org/wiki/Charlieplexing)), with three red LEDs on the bottom to demonstrate unsatisfactory results in that category.  Unsatisfactory results are under 30% of our decided “desirable” values.  For percentage of time facing the class 100% of the time is desired and for student involvement and leaving the podium both were set to 10 times being the desirable quantity for each 10 minute period.  Therefore, since there are 10 LEDs on each bar each LED corresponds to an instance of the professor doing that action (for student involvement and leaving the podium) or 10% in the case of the time facing the students.  Values equal to or over 10 for student involvement and leaving the podium categories result in all the lights being illuminated in the corresponding bar.

The data is collected on one arduino connected to the Pixy and servo.  This data is sent using Wire I2C protocol to another arduino that is connected to the LEDs in the bargraph. The bargraph program controls the LEDs used in two different ways. The first is a simple digitalWrite command, which is used to turn the red “unsatisfactory” LEDs on or off depending on whether the results of the professor’s teaching are above the threshold explained above. The second is the more complex control of the Charlieplexed LEDs. Due to the nature of Charlieplexing, only one LED, of the 30 used to indicate levels of performance, can be illuminated at a time. A 500 microsecond delay is placed between the illumination of each LED in the same way that PWM is done, except to turn multiple LEDs on. Since the proper display of the bargraphs is highly dependent on the time delay between each green LEDs illumination, any sort of delay due to the operation of the tracking program would prevent the bargraphs from working. Subsequently the bargraph program had to be offloaded to the second arduino so that its operation would be uninterrupted by the pauses in the tracking program.



##*Key Results*
Overall the system tracked the three factors we were monitoring well.  As with all systems that depend on the Pixy, it is very susceptible to noise and any information obtained must be verified at a later point.  However overall the results seemed consistent when compared to data recorded by hand.  We tested each component separately and then together in classroom conditions to assure success.  The data outputs at ten minute increments and displays on the bargraphs to give the instructor feedback on their performance.  This system is easy for the professor to see and receive feedback without class being interrupted.  In the future this feedback could easily be extended to be live rather than in ten minute increments. 


##*Lessons Learned*
The exercise in Charlieplexing to produce the bargraphs showed the usefulness of secondary computation systems.  The behavior of the LEDs required us to offload LED control to a secondary arduino. If this system were to be streamlined further the arduino could be replaced by a less complex microcontroller to handle this communication. This method is especially useful when communicating with more complex output devices and can be seen in communication with motors since motor drivers serve the same purpose. This project helped show the feasibility of designing our own “drivers” and the best applications for them.

Figuring out the Wire library for Arduino to Arduino communication was also a big step forward, as we had thought about using it in previous labs but had decided against in favor of keeping our systems simple and relatively small.  This will be very helpful in future projects when arduinos are close enough together to be connected.  That way they can run separate programs at the same time and communicate information. 

##*Results*
###Trial 1:
*** 10 MINUTE MARK ***

Percentage of time facing class: 83.96 %

Number of times left podium: 5

Number of times called on student: 6
###Trial 2:
*** 10 MINUTE MARK ***

Percentage of time facing class: 12.03 %

Number of times left podium: 1

Number of times called on student: 21


#*Images*
Final system set up in a classroom. LED gauge in place with the Pixy mounted on top of a servo mounted on a tripod:
![](raw/final_system.jpg "Final System")

Close up of the bargraph:
![](raw/bargraph.jpg "Bargraph Closeup")

Side of Pixy/servo mounting:
![](raw/camera_mount.jpg "Camera Mount")